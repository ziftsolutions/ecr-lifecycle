ecr-lifecycle
============
The role creates/delets/enable/disable a lifecycle rule for an S3 repo

Requirements
------------
- User needs AWS secret keys

Role Variables
--------------
- repository: (required) ECR repository name
- command: get (default), put or delete
- expire_days: (required for put) Number of days since pushed

Dependencies
------------
None

Example Playbook
----------------
Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    ecr-lifecycle.yml
    - hosts: localhost
      connection: local
      become: no
      roles:
         - ecr-lifecycle

    Examples:
    - Get lifecycle rule for my_ecr_repo
    $ ansible-playbook -i "localhost," ecr-lifecycle.yml \
                       -e ecr_repo=my_ecr_repo

    - Set a lifecycle rule for my_ecr_repo for images > 60 days to be deleted
    $ ansible-playbook -i "localhost," ecr-lifecycle.yml \
                       -e "ecr_repo=my_ecr_repo  \
                           expire_days=60
                           command=set"

    - Delete a lifecycle rule for my_ecr_repo
    $ ansible-playbook -i "localhost," ecr-lifecycle.yml \
                       -e "ecr_repo=my_ecr_repo \
                           command=delete"

License
-------
Zift Solutions

Author Information
------------------
Chris Fouts
